#include <LowPower.h>

#define DOOR_PIN 2
#define ALARM_PIN 9

const int debouncingDelay = 1000;
const int alarmTriggerMillis = 5000;

const int soundMillis = 500;
const int pauseMillis = 500;

const bool production = false;

volatile bool doorMoved = false;

void setup() {
  pinMode(DOOR_PIN, INPUT_PULLUP);
  pinMode(ALARM_PIN, OUTPUT);

  digitalWrite(ALARM_PIN, LOW);

  attachInterrupt(digitalPinToInterrupt(DOOR_PIN), handleDoorInterrupt, CHANGE);
}

void loop() {
  if (doorMoved) {
    handleDoorMoved();
  } else if (production) {
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
  }
}

void handleDoorMoved() {
  unsigned long startTime = millis();
  delay(debouncingDelay);
  doorMoved = false;

  while (isDoorOpen()) {
    unsigned long currentTime = millis();
    if (currentTime > startTime + alarmTriggerMillis) {
      signalAlarm();
    }
  }
}

void signalAlarm() {
  digitalWrite(ALARM_PIN, HIGH);
  delay(soundMillis);
  digitalWrite(ALARM_PIN, LOW);
  delay(pauseMillis);
}

bool isDoorOpen() {
  return digitalRead(DOOR_PIN) == HIGH;
}

void handleDoorInterrupt() {
  doorMoved = true;
}
